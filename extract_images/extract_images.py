import requests
from bs4 import BeautifulSoup
import dropbox


class ImgExtractor(object):
    def __init__(self, url):
        self.url = url

    def __get_html__(self):
        r = requests.get(
            self.url
        )
        return r.content

    @staticmethod
    def __get_image_file_from_src__(img_src_url):
        r = requests.get(img_src_url)
        return r.content

    def get_images(self, largest_only=False):
        html, images, max_size = self.__get_html__(), [], 0
        soup = BeautifulSoup(html, 'html.parser')
        res = soup.find_all("img")
        for item in res:
            img_src = item.attrs.get("src")
            if img_src is not None and img_src.startswith("http"):
                img_file = self.__get_image_file_from_src__(img_src)
                images.append(img_file)
                if largest_only:
                    if len(img_file) > max_size:
                        max_size = len(img_file)

        if largest_only:
            for i in images:
                if len(i) == max_size:
                    images = [i]
                    break

        return images

    def __repr__(self):
        return "ImgExtractor: " + self.url

    @staticmethod
    def upload_to_dropbox(files):
        file_index = 1
        for item in files:
            dbx = dropbox.Dropbox("FckKtgjHMPEAAAAAAAAG2Wp4ldfXHWZ68uE-_xX7gyOoJmNTPyGw-D3kUJ0uIuHC")
            dbx.files_upload(bytes(item), "/file_" + str(file_index) + ".jpg")
            file_index += 1

    @staticmethod
    def upload_largest_file_to_dropbox(*files):
        file_index = 1
        for item in files:
            dbx = dropbox.Dropbox("FckKtgjHMPEAAAAAAAAG2Wp4ldfXHWZ68uE-_xX7gyOoJmNTPyGw-D3kUJ0uIuHC")
            # with open(item, 'rb') as f:
            dbx.files_upload(bytes(item), "/file_" + str(file_index) + ".jpg")


# if __name__ == '__main__':
#     img_extractor = ImgExtractor("https://www.pexels.com/search/nature/")
#     images = img_extractor.get_images()
#     ImgExtractor.upload_to_dropbox(images)
