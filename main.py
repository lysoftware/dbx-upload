"""
This is a script that uploads images (=img tags) from an html page to Lior's DropBox app account
"""
from extract_images.extract_images import ImgExtractor
import sys


def run(url):
    # add url pattern validation
    img_extractor = ImgExtractor(url)
    print (img_extractor)
    images = img_extractor.get_images()  # add argument largest_only=True to upload largest only
    ImgExtractor.upload_to_dropbox(images)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        raise Exception("1 argument allowed for this script")
    try:
        run(sys.argv[1])
    except Exception as e:
        raise e
    #run("https://www.pexels.com/search/nature/")
